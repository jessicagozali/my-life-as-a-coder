# My Life as a Coder

&nbsp;    

>
> Live website link: [https://jessica.webtink.com.au/](https://jessica.webtink.com.au/)
>

&nbsp;    

## Purpose

This website is created for Coder Academy Diversity Scholarship 2021 program for Flex Track Sydney Coding Bootcamp course. A little story about Jessica's journey as a coder, her wish to learn more about software development to create useful things that would make this world a better place.

&nbsp;    

---

&nbsp;    

## Features

&nbsp;    

### CSS Animation

&nbsp;    

![CSS Animations](./docs/css-animations.gif)

&nbsp;    

CSS animations in the website are using either Animate.css for animations and transition effects and Wow.js to reveal animations upon scolling, and plain css translate/transform/animation.

&nbsp;    

### HTML5 Platform Game

&nbsp;    

![2D Platform Game Demo](./docs/phaser-demo.gif)

&nbsp;  

A tiny 2D platform browser game in the website was made using Phaser framework. Most configurations made were for ground, colliders/items, gravity/velocity/movement physics, and sprite frame animation.

&nbsp;    

### Parallax with Gyroscope and/or Cursor Movement Detection

&nbsp;    

![Parallax Effect](./docs/parallax-effect.gif)

&nbsp;  

An awesome radical parallax project by [Matthew Wagerfield](https://matthew.wagerfield.com/) and [Claudio Guglieri](https://cargocollective.com/whydontwetry). The parallax effect responds to motions when the website is opened in devices with motion detection hardware or gyroscope. If opened in web browser, it will react to cursor positioning.

&nbsp;    

## Tech Stack

Template:

- HTML5 Boilerplate: [https://html5boilerplate.com/](https://html5boilerplate.com/)

Framework/Library:

- Animate.css: [https://animate.style/](https://animate.style/)
- Wow.js: [https://wowjs.uk/](https://wowjs.uk/)
- Bootstrap: [https://getbootstrap.com/](https://getbootstrap.com/)
- Phaser: [https://phaser.io/](https://phaser.io/)
- Parallax.js: [https://matthew.wagerfield.com/parallax/](https://matthew.wagerfield.com/parallax/)

Other tool:

- Notepad++: [https://notepad-plus-plus.org/downloads/](https://notepad-plus-plus.org/downloads/)

## Acknowledgements

Template, frameworks/libraries and tool used in the website belong and all credits are given to the respective owner.

Jessica would like to thank Coder Academy for providing her the Diversity Scholarship, giving her the opportunity to leap forward and allowing her to pour her heart and soul in completing the Coding Bootcamp course.
